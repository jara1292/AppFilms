﻿using AppFilms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppFilms.Controllers
{
    public class HomeController : Controller
    {
        private Movie movie = new Movie();
        private Genre genre = new Genre();
        private Actor actor = new Actor();

        public ActionResult Index()
        {
            return View(movie.Listar());
        }

        public ActionResult Ver(int id)
        {
            return View(movie.Obtener(id));
        }

        public ActionResult Crud(int id = 0)
        {
            ViewBag.Genres = genre.Todo();
            ViewBag.Actors = actor.Todo();
            return View(
                id > 0 ? movie.Obtener(id)
                       : movie
            );
        }

        public ActionResult Eliminar(int id)
        {
            movie.Eliminar(id);
            return Redirect("~/home");
        }

        /*public ActionResult Guardar(Alumno model, int[] cursos = null) 
        {
            if (cursos != null)
            {
                foreach (var c in cursos)
                    model.Cursos.Add(new Curso { id = c });
            }
            else 
            {
                ModelState.AddModelError("cursos-elegidos", "Debe seleccionar por lo menos un curso");
            }

            if (ModelState.IsValid)
            {
                model.Guardar();
                return Redirect("~/home/crud/" + model.id);
            }
            else 
            {
                ViewBag.Cursos = curso.Todo();
                return View("~/views/home/crud.cshtml", model);
            }
        }*/

        [HttpPost]
        public JsonResult Guardar(Movie model, int[] generos_seleccionados = null,  int[] actores_seleccionados = null)
        {
            var respuesta = new ResponseModel
            {
                respuesta = true,
                redirect = "/Home/crud/" + model.MovieID,
                error = ""
            };

            if (generos_seleccionados != null)
            {
                foreach (var c in generos_seleccionados)
                    model.Genres.Add(new Genre { GenreID = c });
            }
            else
            {
                ModelState.AddModelError("genres", "Debe seleccionar por lo menos un genero");
                respuesta.respuesta = false;
                respuesta.error = "Debe seleccionar por lo menos un genero";
            }

            if (actores_seleccionados != null)
            {
                foreach (var c in actores_seleccionados)
                    model.Actors.Add(new Actor { ActorID = c });
            }
            else
            {
                ModelState.AddModelError("actors", "Debe seleccionar por lo menos un actor");
                respuesta.respuesta = false;
                respuesta.error2 = "Debe seleccionar por lo menos un actor";
            }

            if (ModelState.IsValid)
            {
                model.Guardar();
            }

            return Json(respuesta);
        }

        //public JsonResult Adjuntar(int Alumno_id, HttpPostedFileBase documento)
        //{
        //    var respuesta = new ResponseModel
        //    {
        //        respuesta = true,
        //        error = ""
        //    };

        //    if (documento != null)
        //    {
        //        string adjunto = DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(documento.FileName);
        //        documento.SaveAs(Server.MapPath("~/uploads/" + adjunto));

        //        this.alumno.Adjuntar(new Adjunto
        //        {
        //            Archivo = adjunto,
        //            Alumno_id = Alumno_id
        //        });
        //    }
        //    else
        //    {
        //        respuesta.respuesta = false;
        //        respuesta.error = "Debe adjuntar un documento";
        //    }

        //    return Json(respuesta);
        //}

        //public PartialViewResult Adjuntos(int Alumno_id)
        //{
        //    return PartialView(alumno.ListarAdjuntos(Alumno_id));
        //}
    }
}