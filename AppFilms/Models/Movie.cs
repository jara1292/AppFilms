﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AppFilms.Models
{
    //public class Movie
    //{
    //    [Key]
    //    public int MovieID { get; set; }

    //    [Required]
    //    [Display(Name="Titulo")]
    //    public string Name { get; set; }

    //    [Required]
    //    [Display(Name = "Año de lanzamiento")]
    //    public DateTime ReleaseYear { get; set; }

    //    [Required]
    //    [Display(Name = "Resumem")]
    //    public string Sinopsis { get; set; }

    //    [Required]
    //    [Display(Name = "Cast")]
    //    public string Cast { get; set; }

    //    [Required]
    //    [Display(Name = "Portada")]
    //    public string Image { get; set; }


    //    public virtual ICollection<MovieGenre> MovieGenres { get; set; }

    //    public virtual ICollection<MovieActor> MovieActors { get; set; }
    //}


    [Table("Movies")]
    public partial class Movie
    {
        public Movie()
        {
            Genres = new List<Genre>();
            Actors = new List<Actor>();
        }

        [Key]
        public int MovieID { get; set; }

        [Required(ErrorMessage = "Debe ingresar el titulo de la pelicula")]
        [Display(Name = "Titulo")]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Año de lanzamiento")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime ReleaseYear { get; set; }

        [Required]
        [Display(Name = "Resumen")]
        public string Sinopsis { get; set; }

        [Required]
        [Display(Name = "Cast")]
        public string Cast { get; set; }

        [Required]
        [Display(Name = "Portada")]
        public string Image { get; set; }

        public ICollection<Genre> Genres { get; set; }

        public ICollection<Actor> Actors { get; set; }
        //public ICollection<Adjunto> Adjuntos { get; set; }

        public List<Movie> Listar()
        {
            var movies = new List<Movie>();
            try
            {
                using (var contexto = new Contexto())
                {
                    movies = contexto.Movies.ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return movies;
        }

        public Movie Obtener(int id)
        {
            var movie = new Movie();
            try
            {
                using (var context = new Contexto())
                {
                    movie = context.Movies
                                    .Include("Genres")
                                    .Include("Actors")
                                    .Where(x => x.MovieID == id)
                                    .Single();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return movie;
        }

        public void Guardar()
        {
            try
            {

                using (var context = new Contexto())
                {
                    if (this.MovieID == 0)
                    {
                        context.Entry(this).State = EntityState.Added;

                        foreach (var c in this.Genres)
                            context.Entry(c).State = EntityState.Unchanged;

                        foreach (var d in this.Actors)
                            context.Entry(d).State = EntityState.Unchanged;
                    }
                    else
                    {
                        this.GuardarGenres();
                        this.GuardarActors();
                        // context.Database.ExecuteSqlCommand(
                        //     "DELETE FROM  GenreMovies WHERE Movie_MovieID = @id",
                        //     new SqlParameter("id", this.MovieID)
                        // );

                        // var genreBK = this.Genres;


                        // this.Genres = null;

                        // context.Entry(this).State = EntityState.Modified;
                        // this.Genres = genreBK;

                        // context.Database.ExecuteSqlCommand(
                        //    "DELETE FROM  MovieActors WHERE Movie_MovieID = @id",
                        //    new SqlParameter("id", this.MovieID)
                        //);
                        // var actorBK = this.Actors;
                        // this.Actors = null;
                        // context.Entry(this).State = EntityState.Modified;
                        // this.Actors = actorBK;



                    }

                    //foreach (var c in this.Genres)
                    //    context.Entry(c).State = EntityState.Unchanged;

                    //foreach (var d in this.Actors)
                    //    context.Entry(d).State = EntityState.Unchanged;

                    context.SaveChanges();


                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }



        public void GuardarGenres()
        {
            try
            {
                using (var context = new Contexto())
                {
                    context.Database.ExecuteSqlCommand(
                             "DELETE FROM  GenreMovies WHERE Movie_MovieID = @id",
                             new SqlParameter("id", this.MovieID)
                         );

                    var genreBK = this.Genres;


                    this.Genres = null;

                    context.Entry(this).State = EntityState.Modified;
                    this.Genres = genreBK;

                    foreach (var c in this.Genres)
                        context.Entry(c).State = EntityState.Unchanged;
                    context.SaveChanges();


                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }


        public void GuardarActors()
        {
            try
            {
                using (var context = new Contexto())
                {

                    context.Database.ExecuteSqlCommand(
                       "DELETE FROM  MovieActors WHERE Movie_MovieID = @id",
                       new SqlParameter("id", this.MovieID)
                   );
                    var actorBK = this.Actors;
                    this.Actors = null;
                    context.Entry(this).State = EntityState.Modified;
                    this.Actors = actorBK;

                    foreach (var d in this.Actors)
                        context.Entry(d).State = EntityState.Unchanged;

                    context.SaveChanges();
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        public void Eliminar(int id)
        {
            try
            {
                using (var context = new Contexto())
                {
                    context.Entry(new Movie { MovieID = id }).State = EntityState.Deleted;
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}