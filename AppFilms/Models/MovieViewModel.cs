﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppFilms.Models
{
    public class MovieViewModel
    {
        public Movie Movie { get; set; }

        public Genre Genre { get; set; }
        public Actor Actor { get; set; }

        public List<Actor> Actors { get; set; }

        public List<MovieGenre> Genres { get; set; }
    }
}