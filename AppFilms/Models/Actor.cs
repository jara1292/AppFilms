﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AppFilms.Models
{

    [Table("Actors")]
    public partial class Actor
    {
        public Actor()
        {
            Movie = new List<Movie>();
        }

        [Key]
        public int ActorID { get; set; }

        [Required]
        [Display(Name = "Nombre de Actor")]
        public string Name { get; set; }

        public ICollection<Movie> Movie { get; set; }

        public List<Actor> Todo()
        {
            var actors = new List<Actor>();
            try
            {
                using (var context = new Contexto())
                {
                    actors = context.Actors.ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return actors;
        }
    }
}