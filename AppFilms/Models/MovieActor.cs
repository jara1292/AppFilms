﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppFilms.Models
{
    public class MovieActor
    {
        [Key]
        public int MovieActorID { get; set; }

        public int MovieID { get; set; }

        public int ActorID { get; set; }

        public virtual Movie Movie { get; set; }
        public virtual Actor Actor { get; set; }
    }
}