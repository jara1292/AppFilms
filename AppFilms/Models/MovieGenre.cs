﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppFilms.Models
{
    public class MovieGenre
    {
        [Key]
        public int MovieGenreID { get; set; }
        public int MovieID { get; set; }
        public int GenreID { get; set; }

        public virtual Movie Movie { get; set; }
        public virtual Genre Genre { get; set; }
    }
}