﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AppFilms.Models
{
    //public class Genre
    //{
    //    [Key]
    //    public int GenreID { get; set; }

    //    [Required]
    //    [Display(Name="Nombre de Genero")]
    //    public string Name { get; set; }

    //    public virtual ICollection<MovieGenre> MovieGenres { get; set; }
    //}


    [Table("Genres")]
    public partial class Genre
    {
        public Genre()
        {
            Movie = new List<Movie>();
        }

        [Key]
        public int GenreID { get; set; }

        [Required]
        [Display(Name = "Nombre de Genero")]
        public string Name { get; set; }

        public ICollection<Movie> Movie { get; set; }

        public List<Genre> Todo()
        {
            var genres = new List<Genre>();
            try
            {
                using (var context = new Contexto())
                {
                    genres = context.Genres.ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return genres;
        }
    }
}