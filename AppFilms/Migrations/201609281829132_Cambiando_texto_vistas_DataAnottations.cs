namespace AppFilms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Cambiando_texto_vistas_DataAnottations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Actors", "FirstName", c => c.String(nullable: false));
            AlterColumn("dbo.Actors", "LastName", c => c.String(nullable: false));
            AlterColumn("dbo.Movies", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Movies", "Sinopsis", c => c.String(nullable: false));
            AlterColumn("dbo.Movies", "Cast", c => c.String(nullable: false));
            AlterColumn("dbo.Movies", "Image", c => c.String(nullable: false));
            AlterColumn("dbo.Genres", "Name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Genres", "Name", c => c.String());
            AlterColumn("dbo.Movies", "Image", c => c.String());
            AlterColumn("dbo.Movies", "Cast", c => c.String());
            AlterColumn("dbo.Movies", "Sinopsis", c => c.String());
            AlterColumn("dbo.Movies", "Name", c => c.String());
            AlterColumn("dbo.Actors", "LastName", c => c.String());
            AlterColumn("dbo.Actors", "FirstName", c => c.String());
        }
    }
}
