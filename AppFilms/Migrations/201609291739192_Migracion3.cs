namespace AppFilms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migracion3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MovieGenres", "GenreID", "dbo.Genres");
            DropForeignKey("dbo.MovieGenres", "MovieID", "dbo.Movies");
            DropIndex("dbo.MovieGenres", new[] { "MovieID" });
            DropIndex("dbo.MovieGenres", new[] { "GenreID" });
            CreateTable(
                "dbo.GenreMovies",
                c => new
                    {
                        Genre_GenreID = c.Int(nullable: false),
                        Movie_MovieID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Genre_GenreID, t.Movie_MovieID })
                .ForeignKey("dbo.Genres", t => t.Genre_GenreID, cascadeDelete: true)
                .ForeignKey("dbo.Movies", t => t.Movie_MovieID, cascadeDelete: true)
                .Index(t => t.Genre_GenreID)
                .Index(t => t.Movie_MovieID);
            
            AlterColumn("dbo.Movies", "Name", c => c.String(nullable: false, maxLength: 50));
            DropTable("dbo.MovieGenres");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MovieGenres",
                c => new
                    {
                        MovieGenreID = c.Int(nullable: false, identity: true),
                        MovieID = c.Int(nullable: false),
                        GenreID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MovieGenreID);
            
            DropForeignKey("dbo.GenreMovies", "Movie_MovieID", "dbo.Movies");
            DropForeignKey("dbo.GenreMovies", "Genre_GenreID", "dbo.Genres");
            DropIndex("dbo.GenreMovies", new[] { "Movie_MovieID" });
            DropIndex("dbo.GenreMovies", new[] { "Genre_GenreID" });
            AlterColumn("dbo.Movies", "Name", c => c.String(nullable: false));
            DropTable("dbo.GenreMovies");
            CreateIndex("dbo.MovieGenres", "GenreID");
            CreateIndex("dbo.MovieGenres", "MovieID");
            AddForeignKey("dbo.MovieGenres", "MovieID", "dbo.Movies", "MovieID", cascadeDelete: true);
            AddForeignKey("dbo.MovieGenres", "GenreID", "dbo.Genres", "GenreID", cascadeDelete: true);
        }
    }
}
