namespace AppFilms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicial : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Movies", "GenreID", "dbo.Genres");
            DropIndex("dbo.Movies", new[] { "GenreID" });
            CreateTable(
                "dbo.MovieActors",
                c => new
                    {
                        MovieActorID = c.Int(nullable: false, identity: true),
                        MovieID = c.Int(nullable: false),
                        ActorID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MovieActorID)
                .ForeignKey("dbo.Actors", t => t.ActorID, cascadeDelete: true)
                .ForeignKey("dbo.Movies", t => t.MovieID, cascadeDelete: true)
                .Index(t => t.MovieID)
                .Index(t => t.ActorID);
            
            CreateTable(
                "dbo.MovieGenres",
                c => new
                    {
                        MovieGenreID = c.Int(nullable: false, identity: true),
                        MovieID = c.Int(nullable: false),
                        GenreID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MovieGenreID)
                .ForeignKey("dbo.Genres", t => t.GenreID, cascadeDelete: true)
                .ForeignKey("dbo.Movies", t => t.MovieID, cascadeDelete: true)
                .Index(t => t.MovieID)
                .Index(t => t.GenreID);
            
            DropColumn("dbo.Movies", "GenreID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Movies", "GenreID", c => c.Int(nullable: false));
            DropForeignKey("dbo.MovieGenres", "MovieID", "dbo.Movies");
            DropForeignKey("dbo.MovieGenres", "GenreID", "dbo.Genres");
            DropForeignKey("dbo.MovieActors", "MovieID", "dbo.Movies");
            DropForeignKey("dbo.MovieActors", "ActorID", "dbo.Actors");
            DropIndex("dbo.MovieGenres", new[] { "GenreID" });
            DropIndex("dbo.MovieGenres", new[] { "MovieID" });
            DropIndex("dbo.MovieActors", new[] { "ActorID" });
            DropIndex("dbo.MovieActors", new[] { "MovieID" });
            DropTable("dbo.MovieGenres");
            DropTable("dbo.MovieActors");
            CreateIndex("dbo.Movies", "GenreID");
            AddForeignKey("dbo.Movies", "GenreID", "dbo.Genres", "GenreID", cascadeDelete: true);
        }
    }
}
