namespace AppFilms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Migracion4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                            "dbo.Actors",
                            c => new
                            {
                                ActorID = c.Int(nullable: false, identity: true),
                                Name = c.String()
                            })
                            .PrimaryKey(t => t.ActorID);


            CreateTable(
                "dbo.MovieActors",
                c => new
                {
                    Movie_MovieID = c.Int(nullable: false),
                    Actor_ActorID = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.Movie_MovieID, t.Actor_ActorID })
                .ForeignKey("dbo.Movies", t => t.Movie_MovieID, cascadeDelete: true)
                .ForeignKey("dbo.Actors", t => t.Actor_ActorID, cascadeDelete: true)
                .Index(t => t.Movie_MovieID)
                .Index(t => t.Actor_ActorID);


        }

        public override void Down()
        {

        }
    }
}
