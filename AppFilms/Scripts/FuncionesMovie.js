﻿var genreItems = new Listas();
var actorItems = new Listas();

function MostrarGenres(editar) {
    $('#genreItems').html('');
    if (genreItems.Total() > 0) {
        var $table = $('<table class="table table-bordered table-striped"/>');
        if (editar)
            $table.append('<thead><tr><th>ID</th><th>Genero</th><th>Opción</th></tr></thead>');
        else
            $table.append('<thead><tr><th>ID</th><th>Genero</th></thead>');
        var $tbody = $('<tbody/>');
        for (var i = 0; i < genreItems.Total() ; i++) {
            console.log(genreItems.Item(i).GenreID);
            var $row = $('<tr/>');
            $row.append($('<td/>').html(genreItems.Item(i).GenreID));
            $row.append($('<td/>').html(genreItems.Item(i).Genre));
            if (editar)
                $row.append($('<td/>').html("<a href='#' class='btn btn-primary' data-toggle='tooltip' title='Eliminar' onClick='return EliminarGenre(" + i + ");'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>"));
            $tbody.append($row);
        }

        $table.append($tbody);
        $('#genreItems').html($table);
    }
}

function MostrarActors(editar) {
    $('#actorsItems').html('');
    if (actorItems.Total() > 0) {
        var $table = $('<table class="table table-bordered table-striped"/>');
        if (editar)
            $table.append('<thead><tr><th>ID</th><th>Nombre</th><th>Opción</th></tr></thead>');
        else
            $table.append('<thead><tr><th>ID</th><th>Nombre</th></tr></thead>');
        var $tbody = $('<tbody/>');
        for (var i = 0; i < actorItems.Total() ; i++) {
            var $row = $('<tr/>');
            $row.append($('<td/>').html(actorItems.Item(i).ActorID));
            $row.append($('<td/>').html(actorItems.Item(i).Actor));
            if (editar)
                $row.append($('<td/>').html("<a href='#' class='btn btn-primary' data-toggle='tooltip' title='Eliminar' onClick='return EliminarActor(" + i + ");'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>"));
            $tbody.append($row);
        }
        $table.append($tbody);
        $('#actorItems').html($table);
    }
}



function addGenre_Click() {
    var isValidItem = true;
    if ($("#GenreID option:selected").html().trim() == '') {
        isValidItem = false;
        $('#GenreID').siblings('span.error').css('visibility', 'visible');
    }
    else {
        $('#GenreID').siblings('span.error').css('visibility', 'hidden');
    }

    if (isValidItem) {
        genreItems.Agregar({
            GenreID: $('#GenreID').val().trim(),
            Genre: $('#GenreID option:selected').html().trim(),
        });
    }

    MostrarGenres(true);
}

function addActor_Click() {
    console.log($('#ActorID').val());
    var isValidItem = true;
    if ($('#ActorID').val().trim() == '') {
        isValidItem = false;
        $('#ActorID').siblings('span.error').css('visibility', 'visible');
    }
    else {
        $('#ActorID').siblings('span.error').css('visibility', 'hidden');
    }

    if (isValidItem) {
        actorItems.Agregar({
            ActorID: $('#ActorID').val().trim(),
            Actor: $('#ActorID option:selected').html().trim(),
        });
    }

    MostrarActors(true);
}




function EliminarGenre(indice) {
    genreItems.Eliminar(indice);
    MostrarGenres(true);
    return false;
}

function EliminarActor(indice) {
    actorItems.Eliminar(indice);
    MostrarActors(true);
    return false;
}


function crear_Click() {
    //validacion del cliente
    var data = {
        Name: $('#Movie_Name').val().trim(),
        ReleaseYear: $('#Movie_ReleaseYear').val().trim(),
        Sinopsis: $('#Movie_Sinopsis').val().trim(),
        Cast: $('#Movie_Cast').val().trim(),
        Image: $('#Movie_Image').val().trim(),
        Genres: genreItems.lista,
        Actors: actorItems.lista
    }
    console.log(data);
    var token = $('[name=__RequestVerificationToken]').val();

    $.ajax({
        url: '/Movies/Create',
        type: "POST",
        data: { __RequestVerificationToken: token, movie: data },
        success: function (d) {
            if (d == true) {
                window.location.href = "/Movies/Index";
            }
            else {
                alert('Hubo un error al momento de guardar');
            }
        },
        error: function () {
            alert('Error, vuelva a intentarlo');
        }
    });
}

function modificar_Click() {
    //validacion del cliente
    var isAllValid = true;

    if ($('#Nombre').val().trim() == '') {
        $('#Nombre').siblings('span.error').css('visibility', 'visible');
        isAllValid = false;
    }
    else {
        $('#Nombre').siblings('span.error').css('visibility', 'hidden');
    }

    if ($('#TipoClienteId').val().trim() == '') {
        $('#TipoClienteId').siblings('span.error').css('visibility', 'visible');
        isAllValid = false;
    }
    else {
        $('#TipoClienteId').siblings('span.error').css('visibility', 'hidden');
    }

    if (isAllValid) {
        var data = {
            ClienteId: $('#ClienteId').val().trim(),
            Nombre: $('#Nombre').val().trim(),
            RFC: $('#RFC').val().trim(),
            TipoPersonaSat: $('#TipoPersonaSat').val().trim(),
            TipoClienteId: $('#TipoClienteId').val().trim(),
            Telefonos: telItems.lista,
            Correos: emailItems.lista,
            Direcciones: direccionesItems.lista
        }

        var idCliente = $('#ClienteId').val().trim();
        var token = $('[name=__RequestVerificationToken]').val();
        var url = '/Clientes/Edit/' + idCliente;
        $.ajax({
            url: url,
            type: "POST",
            data: { __RequestVerificationToken: token, cliente: data },
            success: function (d) {
                if (d == true) {
                    window.location.href = "/Clientes/Index";
                }
                else {
                    alert('Ha ocurrido un error al intentar guardar');
                }
            },
            error: function () {
                alert('Error. Por favor intente de nuevo.');
            }
        });
    }
}





